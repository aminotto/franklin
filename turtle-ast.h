#ifndef TURTLE_AST_H
#define TURTLE_AST_H

#include <stddef.h>
#include <stdbool.h>

// simple commands
enum ast_cmd {
  CMD_UP,
  CMD_DOWN,
  CMD_RIGHT,
  CMD_LEFT,
  CMD_HEADING,
  CMD_FORWARD,
  CMD_BACKWARD,
  CMD_POSITION,
  CMD_HOME,
  CMD_COLOR,
  CMD_PRINT,
};

// internal functions
enum ast_func {
  FUNC_COS,
  FUNC_RANDOM,
  FUNC_SIN,
  FUNC_SQRT,
};

// kind of a node in the abstract syntax tree
enum ast_kind {
  KIND_CMD_SIMPLE,
  KIND_CMD_REPEAT,
  KIND_CMD_BLOCK,
  KIND_CMD_PROC,
  KIND_CMD_CALL,
  KIND_CMD_SET,

  KIND_EXPR_FUNC,
  KIND_EXPR_VALUE,
  KIND_EXPR_BINOP,
  KIND_EXPR_BLOCK,
  KIND_EXPR_NAME,
};

#define AST_CHILDREN_MAX 3

// a node in the abstract syntax tree
struct ast_node {
  enum ast_kind kind; // kind of the node

  union {
    enum ast_cmd cmd;   // kind == KIND_CMD_SIMPLE
    double value;       // kind == KIND_EXPR_VALUE, for literals
    char op;            // kind == KIND_EXPR_BINOP, for operators in expressions
    const char *name;   // kind == KIND_EXPR_NAME, the name of procedures and variables
    enum ast_func func; // kind == KIND_EXPR_FUNC, a function
  } u;

  size_t children_count;  // the number of children of the node
  struct ast_node *children[AST_CHILDREN_MAX];  // the children of the node (arguments of commands, etc)
  struct ast_node *next;  // the next node in the sequence
};


// TODO: make some constructors to use in parser.y
// for example:
struct ast_node *make_expr_value(double value);
struct ast_node *make_expr_name(const char *name);
struct ast_node *make_expr_block(const char *name);
struct ast_node *make_cmd_forward(struct ast_node *expr);
struct ast_node *make_cmd_backward(struct ast_node *expr);
struct ast_node *make_cmd_right(struct ast_node *expr);
struct ast_node *make_cmd_left(struct ast_node *expr);
struct ast_node *make_cmd_heading(struct ast_node *expr);
struct ast_node *make_cmd_color(struct ast_node *expr1, struct ast_node *expr2, struct ast_node *expr3);
struct ast_node *make_cmd_up();
struct ast_node *make_cmd_down();
struct ast_node *make_cmd_home();
struct ast_node *make_cmd_position(struct ast_node *x, struct ast_node *y);

struct ast_node *make_binop(char op, struct ast_node *e1, struct ast_node *e2);
struct ast_node *make_rand(enum ast_func func, struct ast_node *e1, struct ast_node *e2);
struct ast_node *make_expr_func(enum ast_func func, struct ast_node *e1);

struct ast_node *make_block(struct ast_node* cmd);
struct ast_node *make_cmd_repeat(struct ast_node *expr, struct ast_node* block);
struct ast_node *make_cmd_set(struct ast_node *name, struct ast_node *expr);
struct ast_node *make_cmd_proc(struct ast_node *name, struct ast_node *first);
struct ast_node *make_cmd_call(struct ast_node *name);


// root of the abstract syntax tree
struct ast {
  struct ast_node *unit;
};

// do not forget to destroy properly! no leaks allowed!
void ast_destroy(struct ast *self);

// the execution context
struct context {
  double x;
  double y;
  double angle;
  double startAngle;
  bool up;
  double max_x;
  double max_y;
  double min_x;
  double min_y;

  struct variable *variables;
  int sizeVars;
  int nbVars;

  struct procedure *procedures;
  int sizeProcs;
  int nbProcs;

  // TODO: add procedure handling
  // TODO: add variable handling
};

struct variable {
  const char *name;
  double value;
};

struct procedure {
  const char *name;
  struct ast_node *node;
};


// create an initial context
void context_create(struct context *self);

// print the tree as if it was a Turtle program
void ast_print(const struct ast *self);
void ast_node_print(const struct ast_node *node);

// evaluate the tree and generate some basic primitives
double eval_expr(const struct ast_node *node, struct context *ctx);
struct ast_node *eval_block(struct ast_node *node, struct context *ctx);
void ast_eval(const struct ast *self, struct context *ctx);
void ast_eval_node(struct ast_node *n, struct context *ctx);
void eval_command_simple(const struct ast_node *node, struct context *ctx);
double eval_binop(const struct ast_node *node);
void forward(const struct ast_node *node, struct context *ctx);
void backward(const struct ast_node *node, struct context *ctx);
void position(const struct ast_node *node, struct context *ctx);

#endif /* TURTLE_AST_H */
