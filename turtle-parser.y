%{
#include <stdio.h>

#include "turtle-ast.h"

int yylex();
void yyerror(struct ast *ret, const char *);

%}

%debug
%defines

%define parse.error verbose

%parse-param { struct ast *ret }

%union {
  double value;
  const char *name;
  struct ast_node *node;
}

%token <value>    VALUE       "value"
%token <name>     NAME        "name"

%left '+' '-'
%left '*' '/'
%left '^'

%token            KW_FORWARD  "forward"
%token            KW_BACKWARD  "backward"
%token            KW_RIGHT  "right"
%token            KW_LEFT  "left"
%token            KW_HEADING  "heading"
%token            KW_UP  "up"
%token            KW_DOWN  "down"
%token            KW_HOME  "home"
%token            KW_POSITION  "position"
%token            KW_SET  "set"
%token            KW_PROC  "proc"
%token            KW_CALL  "call"
%token            KW_REPEAT  "repeat"
%token            KW_COLOR  "color"
%token            KW_SIN  "sin"
%token            KW_COS  "cos"
%token            KW_TAN  "tan"
%token            KW_SQRT  "sqrt"
%token            KW_RANDOM  "random"
%token            RED  "red"
%token            GREEN  "green"
%token            BLUE  "blue"
%token            BLACK  "black"
%token            GREY  "grey"
%token            CYAN  "cyan"
%token            YELLOW  "yellow"
%token            MAGENTA  "magenta"



/* TODO: add other tokens */

%type <node> unit cmds cmd expr block

%%

unit:
    cmds              { $$ = $1; ret->unit = $$; }
;

cmds:
    cmd cmds          { $1->next = $2; $$ = $1; }
    | block           {$$ = $1;}
    | /* empty */     { $$ = NULL; }
;

cmd:
    KW_FORWARD expr   { $$ = make_cmd_forward($2); }
    | KW_BACKWARD expr   { $$ = make_cmd_backward($2); }
    | KW_RIGHT expr   { $$ = make_cmd_right($2); }
    | KW_LEFT expr   { $$ = make_cmd_left($2); }
    | KW_UP                 { $$ = make_cmd_up(); }
    | KW_DOWN               { $$ = make_cmd_down(); }
    | KW_HOME               { $$ = make_cmd_home(); }
    | KW_POSITION expr ',' expr { $$ = make_cmd_position($2, $4); }
    | KW_HEADING expr    { $$ = make_cmd_heading($2); }
    | KW_COLOR expr ',' expr ',' expr {$$ = make_cmd_color($2, $4, $6);}
    | KW_COLOR RED       { $$ = make_cmd_color(make_expr_value(1.0), make_expr_value(0.0), make_expr_value(0.0)); }
    | KW_COLOR GREEN     { $$ = make_cmd_color(make_expr_value(0.0), make_expr_value(1.0), make_expr_value(0.0)); }
    | KW_COLOR BLUE      { $$ = make_cmd_color(make_expr_value(0.0), make_expr_value(0.0), make_expr_value(1.0)); }
    | KW_COLOR BLACK     { $$ = make_cmd_color(make_expr_value(0.0), make_expr_value(0.0), make_expr_value(0.0)); }
    | KW_COLOR GREY      { $$ = make_cmd_color(make_expr_value(0.5), make_expr_value(0.5), make_expr_value(0.5)); }
    | KW_COLOR CYAN      { $$ = make_cmd_color(make_expr_value(0.0), make_expr_value(1.0), make_expr_value(1.0)); }
    | KW_COLOR YELLOW    { $$ = make_cmd_color(make_expr_value(1.0), make_expr_value(0.0), make_expr_value(1.0)); }
    | KW_COLOR MAGENTA   { $$ = make_cmd_color(make_expr_value(1.0), make_expr_value(1.0), make_expr_value(0.0)); }
    | KW_REPEAT expr block    { $$ = make_cmd_repeat($2, $3); }
    | KW_SET NAME expr   { $$ = make_cmd_set(make_expr_name($2), $3); }
    | KW_PROC NAME block   { $$ = make_cmd_proc(make_expr_name($2), $3); }
    | KW_CALL NAME   { $$ = make_cmd_call(make_expr_name($2)); }
;

expr:
    VALUE               { $$ = make_expr_value($1); }
    | NAME              { $$ = make_expr_name($1); }
    | expr '+' expr     { $$ = make_binop('+',$1,$3);}
    | expr '-' expr     { $$ = make_binop('-',$1,$3);}
    | expr '*' expr     { $$ = make_binop('*',$1,$3);}
    | expr '/' expr     { $$ = make_binop('/',$1,$3);}
    | expr '^' expr     { $$ = make_binop('^',$1,$3);}
    | '(' expr ')'      { $$ = $2; }
    | '-' expr          { $$ = make_binop('-',make_expr_value(0),$2);}
    | KW_SQRT '(' expr ')'      { $$ = make_expr_func(FUNC_SQRT, $3);}
    | KW_SIN '(' expr ')'       { $$ = make_expr_func(FUNC_SIN, $3);}
    | KW_COS '(' expr ')'       { $$ = make_expr_func(FUNC_COS, $3);}
    | KW_RANDOM '(' expr ',' expr ')'    { $$ = make_rand(FUNC_RANDOM, $3, $5);}

    /* TODO: add identifier */
;

block:
    '{' cmds '}'       { $$ = make_block($2); }
    | cmd              { $$ = make_block($1); }
    | NAME             { $$ = make_expr_block($1); }
;
  

%%

void yyerror(struct ast *ret, const char *msg) {
  (void) ret;
  fprintf(stderr, "%s\n", msg);
}
