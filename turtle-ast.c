#include "turtle-ast.h"
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define PI 3.141592653589793
#define SQRT2 1.41421356237309504880
#define SQRT3 1.7320508075688772935

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

struct ast_node *make_expr_value(double value) {
  struct ast_node *node = calloc(1, sizeof(struct ast_node));
  node->kind = KIND_EXPR_VALUE;
  node->u.value = value;
  node->children_count = 0;
  return node;
}

struct ast_node *make_expr_name(const char *name) {
  struct ast_node *node = calloc(1, sizeof(struct ast_node));
  node->kind = KIND_EXPR_NAME;
  node->u.name = name;
  node->children_count = 0;
  return node;
}

struct ast_node *make_expr_block(const char *name) {
  struct ast_node *node = calloc(1, sizeof(struct ast_node));
  node->kind = KIND_EXPR_BLOCK;
  node->u.name = name;
  node->children_count = 0;
  return node;
}

struct ast_node *make_cmd_forward(struct ast_node *expr) {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_SIMPLE;
	node->u.cmd = CMD_FORWARD;
	node->children_count = 1;
	node->children[0] = expr;
	return node;
}

struct ast_node *make_cmd_backward(struct ast_node *expr) {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_SIMPLE;
	node->u.cmd = CMD_BACKWARD;
	node->children_count = 1;
	node->children[0] = expr;
	return node;
}

struct ast_node *make_cmd_right(struct ast_node *expr) {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_SIMPLE;
	node->u.cmd = CMD_RIGHT;
	node->children_count = 1;
	node->children[0] = expr;
	return node;
}

struct ast_node *make_cmd_left(struct ast_node *expr) {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_SIMPLE;
	node->u.cmd = CMD_LEFT;
	node->children_count = 1;
	node->children[0] = expr;
	return node;
}


struct ast_node *make_cmd_color(struct ast_node *expr1, struct ast_node *expr2, struct ast_node *expr3) {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_SIMPLE;
	node->u.cmd = CMD_COLOR;
	node->children_count = 3;
	node->children[0] = expr1;
	node->children[1] = expr2;
	node->children[2] = expr3;
	return node;
}

struct ast_node *make_cmd_up() {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_SIMPLE;
	node->u.cmd = CMD_UP;
	node->children_count = 0;
	return node;
}

struct ast_node *make_cmd_down() {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_SIMPLE;
	node->u.cmd = CMD_DOWN;
	node->children_count = 0;
	return node;
}

struct ast_node *make_cmd_home() {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_SIMPLE;
	node->u.cmd = CMD_HOME;
	node->children_count = 0;
	return node;
}

struct ast_node *make_cmd_position(struct ast_node *x, struct ast_node *y) {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_SIMPLE;
	node->u.cmd = CMD_POSITION;
	node->children_count = 2;
	node->children[0] = x;
	node->children[1] = y;
	return node;
}

struct ast_node *make_cmd_heading(struct ast_node *expr) {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_SIMPLE;
	node->u.cmd = CMD_HEADING;
	node->children_count = 1;
	node->children[0] = expr;
	return node;
}

struct ast_node *make_binop(char op, struct ast_node *e1, struct ast_node *e2){
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_EXPR_BINOP;
	node->u.op = op;
	node->children_count = 2;
	node->children[0] = e1;
	node->children[1] = e2;
	return node;
}

struct ast_node *make_rand(enum ast_func func, struct ast_node *e1, struct ast_node *e2){
	struct ast_node *node = make_expr_func(func, e1);
	node->children_count++;
	node->children[1] = e2;
	return node;
}

struct ast_node *make_expr_func(enum ast_func func, struct ast_node *e1){
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_EXPR_FUNC;
	node->u.func = func;
	node->children_count = 1;
	node->children[0] = e1;
	return node;
}

struct ast_node *make_block(struct ast_node* cmd) {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_BLOCK;
 	node->children_count=1;
  	node->children[0]=cmd;
	return node;
}

struct ast_node *make_cmd_repeat(struct ast_node *expr, struct ast_node* block) {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_REPEAT;
 	node->children_count=2;
  	node->children[0]=expr;
  	node->children[1] = block;
	return node;
}

struct ast_node *make_cmd_set(struct ast_node *name, struct ast_node *expr) {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_SET;
	node->children_count = 2;
	node->children[0] = name;
	node->children[1] = expr;
	return node;
}

struct ast_node *make_cmd_proc(struct ast_node *name, struct ast_node *first) {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_PROC;
	node->children_count = 2;
	node->children[0] = name;
	node->children[1] = first;
	return node;
}

struct ast_node *make_cmd_call(struct ast_node *name) {
	struct ast_node *node = calloc(1, sizeof(struct ast_node));
	node->kind = KIND_CMD_CALL;
	node->children_count = 1;
	node->children[0] = name;
	return node;
}


void ast_destroy(struct ast *self) {

}

/*
 * context
 */

void context_create(struct context *self) {
	self->x = 0;
	self->y = 0;
	self->startAngle = 3*PI/2;
	self->angle = self->startAngle;
	self->up = false;
	self->min_x = -500;
	self->max_x = 500;
	self->min_y = -500;
	self->max_y = 500;

	self->sizeVars = 1;
	self->variables = malloc(sizeof(struct variable));
	self->nbVars = 0;

	self->sizeProcs = 1;
	self->procedures = malloc(sizeof(struct procedure));
	self->nbProcs = 0;
}

/*
 * eval
 */

void ast_eval(const struct ast *self, struct context *ctx) {
	struct ast_node *node = self->unit;
	ast_eval_node(node, ctx);
}

void ast_eval_node(struct ast_node *n, struct context *ctx) {
	struct ast_node *node = n;
	do {
		switch(node->kind) {
			case KIND_CMD_SIMPLE:
				eval_command_simple(node, ctx);
				break;
			case KIND_CMD_REPEAT:
				for(int i=0; i<(int)eval_expr(node->children[0], ctx); i++) {
					ast_eval_node(eval_block(node->children[1], ctx), ctx);
				}
				break;
			case KIND_CMD_BLOCK:
				ast_eval_node(node->children[0], ctx);
				break;
			case KIND_CMD_PROC: {
				
				bool found = false;

				for (int i=0; i < ctx->nbProcs; i++){
					if (strcmp(ctx->procedures[i].name, node->children[0]->u.name) == 0){
						found = true;
						ctx->procedures[i].node = node->children[1];
					}
				}

				if (!found){
					ctx->procedures[ctx->nbProcs].name = node->children[0]->u.name;
					ctx->procedures[ctx->nbProcs++].node = node->children[1];

					if (ctx->nbProcs == ctx->sizeProcs){
						ctx->sizeProcs *= 2;
						ctx->procedures = realloc(ctx->procedures, ctx->sizeProcs * sizeof(struct procedure));
					}
				}

				break;
			}
			case KIND_CMD_CALL:
				for (int i=0; i < ctx->nbProcs; i++){
						if (strcmp(ctx->procedures[i].name, node->children[0]->u.name) == 0){
							ast_eval_node(ctx->procedures[i].node, ctx);
						}
				}
				break;
			case KIND_CMD_SET:{
				
				bool found = false;

				for (int i=0; i < ctx->nbVars; i++){
					if (strcmp(ctx->variables[i].name, node->children[0]->u.name) == 0){
						found = true;
						ctx->variables[i].value = eval_expr(node->children[1], ctx);
					}
				}

				if (!found){
					ctx->variables[ctx->nbVars].name = node->children[0]->u.name;
					ctx->variables[ctx->nbVars++].value = eval_expr(node->children[1], ctx);

					if (ctx->nbVars == ctx->sizeVars){
						ctx->sizeVars *= 2;
						ctx->variables = realloc(ctx->variables, ctx->sizeVars * sizeof(struct variable));
					}
				}

				break;
			}
		}
	} while((node = node->next)!=NULL);
}

void eval_command_simple(const struct ast_node *node, struct context *ctx) {
	switch(node->u.cmd) {
		case CMD_UP:
			ctx->up = true;
			break;
		case CMD_DOWN:
			printf("MoveTo %.6f %.6f\n",ctx->x, ctx->y);
			ctx->up = false;
			break;
		case CMD_RIGHT:
			ctx->angle += (eval_expr(node->children[0], ctx)/360.)*2*PI;
			break;
		case CMD_LEFT:
			ctx->angle -= (eval_expr(node->children[0], ctx)/360.)*2*PI;
			break;
		case CMD_HEADING:
			ctx->angle = ctx->startAngle + (eval_expr(node->children[0], ctx)/360.)*2*PI;
			break;
		case CMD_FORWARD:
			forward(node, ctx);
			break;
		case CMD_BACKWARD:
			backward(node, ctx);
			break;
		case CMD_POSITION:
			position(node, ctx);
			break;
		case CMD_HOME:
			ctx->x = 0;
			ctx->y = 0;
			ctx->angle = ctx->startAngle;
			ctx->up = false;
			break;
		case CMD_COLOR:
			printf("Color  %.6f %.6f %.6f\n", eval_expr(node->children[0], ctx),eval_expr(node->children[1], ctx),eval_expr(node->children[2], ctx));
			break;
		case CMD_PRINT:
			break;
	}
}

struct ast_node *eval_block(struct ast_node *node, struct context *ctx) {
	switch (node->kind) {
		case KIND_EXPR_BLOCK:
			for (int i = 0; i < ctx->nbProcs; i++){
					if (strcmp(ctx->procedures[i].name, node->u.name) == 0){
						return ctx->procedures[i].node;
					}
				}
				return NULL;
			break;
		default:
			return node;
	}
}


double eval_expr(const struct ast_node *node, struct context *ctx){

	switch (node->kind) {
		case KIND_EXPR_BINOP:
		{
				double child1 = eval_expr(node->children[0], ctx);
				double child2 = eval_expr(node->children[1], ctx);

				switch(node->u.op) {
						case '+':
							return child1+child2;
						case '-':
							return child1-child2;
						case '*':
							return child1*child2;
						case '/':
							return child1/child2;
						case '^':
							return pow(child1, child2);
						default:
							return 0.0;
				}
				break;
		}
		case KIND_EXPR_FUNC:
				switch (node->u.func) {
					case FUNC_COS:
							return cos(eval_expr(node->children[0], ctx));
						break;
					case FUNC_SIN:
							return sin(eval_expr(node->children[0], ctx));
						break;
					case FUNC_SQRT:
							return sqrt(eval_expr(node->children[0], ctx));
						break;
					case FUNC_RANDOM:{
							double a = eval_expr(node->children[0], ctx);
							double b = eval_expr(node->children[1], ctx);
							double random = ((double) rand()) / (double) RAND_MAX;
							double diff = b - a;
							double r = random * diff;
							return (a + r);
						break;
					}
					default:
					return 0.0;
				}
				break;
		case KIND_EXPR_VALUE:
				return node->u.value;
		case KIND_EXPR_NAME:
				//Ici faudra qu'on récupère dans notre contexte la valeur de la variable de nom node->u.name
				for (int i = 0; i < ctx->nbVars; i++){
					if (strcmp(ctx->variables[i].name, node->u.name) == 0){
						return ctx->variables[i].value;
					}
				}
				return 0.0;
		default:
			return 0.0;
	}
}


void forward(const struct ast_node *node, struct context *ctx) {
	struct ast_node *expr = node->children[0];

	float newX = ctx->x + eval_expr(expr, ctx) * cos(ctx->angle);
	newX = min(ctx->max_x, newX);
	newX = max(ctx->min_x, newX);

	float newY = ctx->y + eval_expr(expr, ctx) * sin(ctx->angle);
	newY = min(ctx->max_y, newY);
	newY = max(ctx->min_y, newY);

	if(!ctx->up) {
		printf("LineTo %.6f %.6f\n",newX, newY);
	}
	ctx->x = newX;
	ctx->y = newY;
}

void backward(const struct ast_node *node, struct context *ctx) {
	struct ast_node *expr = node->children[0];

	float newX = ctx->x - eval_expr(expr, ctx) * cos(ctx->angle);
	newX = min(ctx->max_x, newX);
	newX = max(ctx->min_x, newX);

	float newY = ctx->y - eval_expr(expr, ctx) * sin(ctx->angle);
	newY = min(ctx->max_y, newY);
	newY = max(ctx->min_y, newY);

	if(!ctx->up) {
		printf("LineTo %.6f %.6f\n",newX, newY);
	}
	ctx->x = newX;
	ctx->y = newY;
}

void position(const struct ast_node *node, struct context *ctx){
	struct ast_node *x = node->children[0];
	struct ast_node *y = node->children[1];

	float newX = min(eval_expr(x, ctx), ctx->max_x);
	newX = max(eval_expr(x, ctx), ctx->min_x);

	float newY = min(eval_expr(y, ctx), ctx->max_y);
	newY = max(eval_expr(y, ctx), ctx->min_y);

	printf("MoveTo %.6f %.6f\n",newX, newY);
	ctx->x = newX;
	ctx->y = newY;
}

/*
 * print
 */

void ast_print(const struct ast *self) {
	ast_node_print(self->unit);
}

void ast_node_print(const struct ast_node *node) {
	printf("node kind : %d\n",(int)(node->kind));
	printf("command : %d\n",(int)(node->u.cmd));
	printf("value : %f\n", node->u.value);
	printf("operator : %c\n", node->u.op);
	printf("function : %d\n", (int)(node->u.func));
	printf("number of childrens : %lu\n", node->children_count);
	printf("---------------------------------------------------\n");
	if(node->children_count>0) {
		for(int i=0; i<node->children_count; i++) {
			printf("Children %d :\n", i);
			ast_node_print(node->children[i]);
		}
	}
	printf("---------------------------------------------------\n");
	if(node->next!=NULL)
		ast_node_print(node->next);
}
