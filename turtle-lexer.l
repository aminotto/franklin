%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "turtle-ast.h"
#include "turtle-parser.h"
%}

%option warn 8bit nodefault noyywrap

DIGIT [0-9]

COMM      #.*\n
INTEGER   0|([1-9][0-9]*)
FLOAT     ({INTEGER}\.{DIGIT}*)|({INTEGER}?\.{DIGIT}+)
FLOAT_E   ({FLOAT}|{INTEGER})[eE][+-]?({FLOAT}|{INTEGER})+
VAR       [A-Z]([A-Z0-9])*

%%

{DIGIT}+              { yylval.value = strtod(yytext, NULL); return VALUE; }
{FLOAT}|{FLOAT_E}     { yylval.value = atof(yytext); return VALUE; }
{VAR}                 { yylval.name = strdup(yytext); return NAME; }

"+"                   { return '+'; }
"-"                   { return '-'; }
"*"                   { return '*'; }
"/"                   { return '/'; }
"^"                   { return '^'; }
"("                   { return '('; }
")"                   { return ')'; }
"{"                   { return '{'; }
"}"                   { return '}'; }
","                   { return ','; }

"forward"|"fw"        { return KW_FORWARD; }
"backward"|"bw"       { return KW_BACKWARD; }
"right"|"rt"          { return KW_RIGHT; }
"left"|"lt"           { return KW_LEFT; }
"heading"|"hd"        { return KW_HEADING; }
"home"                { return KW_HOME; }
"position"|"pos"      { return KW_POSITION; }
"set"				  { return KW_SET; }
"proc"				  { return KW_PROC; }
"repeat"			  { return KW_REPEAT; }
"call"			      { return KW_CALL; }
"color"               { return KW_COLOR; }

"up"                  { return KW_UP; }
"down"                { return KW_DOWN; }

"sin"                 { return KW_SIN; }
"cos"                 { return KW_COS; }
"tan"                 { return KW_TAN; }
"sqrt"                { return KW_SQRT; }
"random"              { return KW_RANDOM; }

"red"                 { return RED; }
"green"               { return GREEN; }
"blue"                { return BLUE; }
"black"               { return BLACK; }
"grey"                { return GREY; }
"cyan"                { return CYAN; }
"yellow"              { return YELLOW; }
"magenta"             { return MAGENTA; }

{COMM}
[\n\t ]*              /* whitespace */
.                     { fprintf(stderr, "Unknown token: '%s'\n", yytext); exit(EXIT_FAILURE); }

%%
